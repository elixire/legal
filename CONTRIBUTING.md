# Contributing to elixi.re's legal documents

We welcome your work on fixing typos or making wording
cleaner on our documents through GitLab Issues and GitLab Merge Requests.
